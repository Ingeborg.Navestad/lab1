package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);

        multiplicationTable(4);

        crossSum(12);

    }

    public static void multiplesOfSevenUpTo(int n) {
        int num = 7; 
        while (num <= n) {
            System.out.println(num); 
            num += 7; 
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i ++) {
            System.out.print(i+": ");
            for (int j = 1; j <= n; j ++) {
                int number = (j * i); 
                System.out.print(number + " ");

            }
            System.out.println();
        }

    }

    public static int crossSum(int num) {
        int total = 0;
        while (num > 0) { 
            int add = num % 10; 
            total += add; 
            num -= add;
            num /= 10; 
        }
        
        return total; 
        }
    
    }

