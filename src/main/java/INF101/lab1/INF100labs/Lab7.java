package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different input

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        for (ArrayList<Integer> currentRow : grid) { 
            if (grid.indexOf(currentRow) == row) { 
                grid.remove(currentRow); 
                break;
            }
        }
        }


    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int sumFirstRow = 0; 
        ArrayList<Integer> firstRow = new ArrayList<>();
        firstRow = grid.get(0); 
        for (int elem : firstRow) { 
            sumFirstRow += elem; 
        }
        

        for (ArrayList<Integer> row : grid) {
            int sumRow = 0; 
            for (int elem : row) {
                sumRow += elem; 
            }
            if (sumRow != sumFirstRow) {
                return false;
        } 
        }
    
        int sumFirstCol = 0; 
        for (ArrayList<Integer> currentRow : grid) {
            sumFirstCol += currentRow.get(0);
        }

        for (int i = 0; i < grid.size(); i ++) {
            int sumCol = 0;
            for (ArrayList<Integer> currentRow : grid) {
                sumCol += currentRow.get(i);
            }
            if (sumCol != sumFirstCol) {
                return false;
            }
        }

        return true; 

    }

}