package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> dobbelListe = new ArrayList<>(); 
        for (int elem : list) { 
            int dobbel = elem * 2; 
            dobbelListe.add(dobbel);
        }

        return dobbelListe;

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> utenTre = new ArrayList<>(); 
        for (int elem : list) { 
            if (elem != 3) {
                utenTre.add(elem);
            }
        }
        return utenTre;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
       ArrayList<Integer> unikeVerdier = new ArrayList<>(); 
       for (int elem : list) { 
            if (!unikeVerdier.contains(elem)) {
                unikeVerdier.add(elem);
            }
       }

       return unikeVerdier;
    }



    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i ++) { 
            int elemA = a.get(i);
            int elemB = b.get(i); 
            int sum = elemA + elemB; 
            a.set(i, sum); 
        }
    }


}


